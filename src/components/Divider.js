import React from 'react';

class Divider extends React.Component{
    
    render(){
        var date = this.props.date;
        return(
            <div className="messages-divider">
                {date}
            </div>
        )
    }
        
}      


export default Divider;