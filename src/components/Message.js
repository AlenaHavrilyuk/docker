import React from 'react';
import '../css/message.css'
import LikeButton from './likeButton';

class Message extends React.Component{
    render(){
        var data = this.props.message;
        var edited = "";
        var d;
        if(data.editedAt === "") {
            d = new Date(data.createdAt);
        } else{
            d = new Date(data.editedAt);
            edited = "edited";
        }
        let time = new Intl.DateTimeFormat('en', { hour: '2-digit', minute: '2-digit', hour12: false}).format(d);
        return(
        <div className="message">
            <div className="message-text">
                {data.text}
            </div>
            <div className="message-time">
                {time} {edited}
            </div>
            <LikeButton />
            <div className="message-user-name">
                {data.user}
            </div>
            <div className="message-user-avatar">
                <img src={data.avatar} alt="../images/no-avatar.png"/>
            </div>
            <div className="message-background"></div>
        </div>
        )
    }
}

export default Message;